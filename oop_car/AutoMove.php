<?php

use lib\auto\MovableInterface;

require "vendor/autoload.php";

class AutoMove implements MovableInterface
{
    const UP_STEP = 20;
    protected $currentSpeed = 0;
    public $maxSpeed = 260;
    /**
     * @inheritDoc
     */
    public function start()
    {
        echo "Зажигание включено". "<br>" . "\n";
    }

    /**
     * @inheritDoc
     */
    public function stop()
    {
        echo "Зажигание выключено" . "<br>";
    }

    /**
     * @inheritDoc
     */
    public function up()
    {
        while ($this->currentSpeed < $this->maxSpeed) {
            $this->currentSpeed += self::UP_STEP;
            echo "Скорость автомобиля " . $this->currentSpeed . " км/час" . "<br>" . "\n";
            sleep(1);
        }
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        while ($this->currentSpeed = $this->maxSpeed) {
            $this->maxSpeed -= self::UP_STEP;
            echo "Скорость автомобиля " . $this->maxSpeed . " км/час" . "<br>". "\n";
            sleep(1);
        }
    }
}